namespace ProyectoITA.Models
{
    public class LoginModel
    {
        public string User { get; set; }
        public string Pass { get; set; }
    }
}