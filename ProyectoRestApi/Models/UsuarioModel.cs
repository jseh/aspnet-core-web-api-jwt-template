using System;

namespace ProyectoITA.Models
{
    public class UsuarioModel
    {
        public string Nombre { get; set; }
        public string Email { get; set; }
        public DateTime Birthdate { get; set; }
    }
}