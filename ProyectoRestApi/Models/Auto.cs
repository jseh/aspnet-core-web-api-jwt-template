﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoRestApi.Models
{
    public class Auto
    {
        public string Modelo { get; set; }
        public string Marca { get; set; }
    }
}
