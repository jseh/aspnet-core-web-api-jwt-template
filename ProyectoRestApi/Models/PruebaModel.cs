﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoRestApi.Models
{
    public class PruebaModel
    {
        [Required]
        public string Nombre { get; set; }

        public int Edad { get; set; }
    }
}
