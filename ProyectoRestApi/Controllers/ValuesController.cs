﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProyectoRestApi.Models;

namespace ProyectoRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        /*
            Get, Post, Put, Delete estan reservados para el controlador          
        */

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }
        
        

        // POST api/values
        [HttpPost]
        public string Post([FromBody] PruebaModel m)
        {
            return m.Nombre;
        }
        
        //// POST api/values
        //[HttpPost]
        //public string Nn([FromBody] PruebaModel m)
        //{
        //    return m.Nombre;
        //}

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
