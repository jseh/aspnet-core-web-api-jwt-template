﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ProyectoITA.Models;

namespace ProyectoRestApi.Controllers
{
    // api/auth/
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Token(LoginModel login)
        {
            IActionResult response = Unauthorized();
            var usuarioid = "a@a.com";

            // Autenticar busca al usuario en la bd y comprueba sus credenciales
            var usuario = Autenticar(login);
            if (usuario != null)
            {
                var tokenString = GenerarToken(usuario);
                var tokenRefreshString = GenerarRefreshToken(usuarioid);
                response = Ok(
                    new {
                            token = tokenString,
                            tokenRefresh = tokenRefreshString
                        }
               );
            }

            return response;
        }

        [AllowAnonymous]
        [HttpPost("refresh")]
        public IActionResult Refresh(string refreshToken)
        {
            IActionResult response = BadRequest();

            // Checa que el token no haya sido modificado, expirado y lo abre
            var principal = ValidarAbrirRefreshToken(refreshToken);
            // extraer el claim usuarioid desde principal
            var usuarioid = "a@a.com";
            var savedRefreshToken = ObtenerRefreshTokenDeBD(usuarioid); // recuperar el token de la bd  y verificar que exista y sea igual al recibido
            // Si el token recibido, no concuerda con el de la BD, salir con error 400
            if (savedRefreshToken != refreshToken)
            {
                throw new SecurityTokenException("Invalid refresh token");
                //return response;
            }
            else
            {
                var nuevoUsuario = new UsuarioModel()
                {
                    Nombre = "",
                    Email = "a@a.com"

                };

                string newJwtToken = GenerarToken(nuevoUsuario);
                string newRefreshToken = GenerarRefreshToken(usuarioid);

                //Borrar el token anterior en la BD y almacenar el nuevo
                //DeleteRefreshToken(username, refreshToken);
                //SaveRefreshToken(username, newRefreshToken);

                return Ok(
                        new
                        {
                            token = newJwtToken,
                            refreshToken = newRefreshToken
                        }
                    );



            }

            // Retorna Error 400 si no pasa la validacion
            //return response;
        }

        private ClaimsPrincipal ValidarAbrirRefreshToken(string refreshToken)
        {

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("the server key used to sign the JWT token is here, use more than 16 chars")),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();


            SecurityToken securityToken;
            // valida el token
            ClaimsPrincipal principal = tokenHandler.ValidateToken(refreshToken, tokenValidationParameters, out securityToken);

            // revisa si el token es valido segun tokenValidationParameters y si se esta usando el algoritmo HS256
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        public string GenerarToken(UsuarioModel usuario)
        {
            /*
             el modelo de usuario que es pasado aquí
             es utilizado para rellenar los claims 
            */
            var misClaims = new[] {
                // new Claim(JwtRegisteredClaimNames.Sub, user.Name),
                // new Claim(JwtRegisteredClaimNames.Email, user.Email),
                // new Claim(JwtRegisteredClaimNames.Birthdate, user.Birthdate.ToString("yyyy-MM-dd")),
                // new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("TipoUsuario","Alumno"),
                //new Claim("NoControl","13221141"),
                new Claim("Nombre",usuario.Nombre),
                new Claim("Email",usuario.Email),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("kjhdlahjdsfaskuiedfqpwfhqweifqñwefji29302139dj23dj293d0jfjasdifjañ"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "Jseh",
                audience: "Lectores",
                expires: DateTime.Now.AddMinutes(1),
                signingCredentials: creds,
                claims: misClaims
            );

            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        public string GenerarRefreshToken(string usuarioid)
        {
            // Genera un nuevo RefreshToken
            // Agrega Informacion para identificar al usuario
            var misClaims = new[] {
                new Claim("UsuarioId", usuarioid),             
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("kjhdlahjdsfaskuiedfqpwfhqweifqñwefji29302139dj23dj293d0jfjasdifjañ"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "Jseh",
                audience: "Lectores",
                expires: DateTime.Now.AddMinutes(2),
                signingCredentials: creds,
                claims: misClaims
            );

            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        public string ObtenerRefreshTokenDeBD(string token)
        {
            return "ahsdjfkhaklsdhfalksdhfjkasdhf";
        }


        private static JwtSecurityToken ValidateAndDecode(string jwt, IEnumerable<SecurityKey> signingKeys)
        {
            var validationParameters = new TokenValidationParameters
            {
                // Clock skew compensates for server time drift.
                // We recommend 5 minutes or less:
                ClockSkew = TimeSpan.FromMinutes(5),
                // Specify the key used to sign the token:
                IssuerSigningKeys = signingKeys,
                RequireSignedTokens = true,
                // Ensure the token hasn't expired:
                RequireExpirationTime = true,
                ValidateLifetime = true,
                // Ensure the token audience matches our audience value (default true):
                ValidateAudience = true,
                ValidAudience = "api://default",
                // Ensure the token was issued by a trusted authorization server (default true):
                ValidateIssuer = true,
                ValidIssuer = "https://{yourOktaDomain}/oauth2/default"
            };

            try
            {
                var claimsPrincipal = new JwtSecurityTokenHandler()
                    .ValidateToken(jwt, validationParameters, out var rawValidatedToken);

                return (JwtSecurityToken)rawValidatedToken;
                // Or, you can return the ClaimsPrincipal
                // (which has the JWT properties automatically mapped to .NET claims)
            }
            catch (SecurityTokenValidationException stvex)
            {
                // The token failed validation!
                // TODO: Log it or display an error.
                throw new Exception($"Token failed validation: {stvex.Message}");
            }
            catch (ArgumentException argex)
            {
                // The token was not well-formed or was invalid for some other reason.
                // TODO: Log it or display an error.
                throw new Exception($"Token was invalid: {argex.Message}");
            }
        }

        public UsuarioModel Autenticar(LoginModel login)
        {
            UsuarioModel usuario = null;
            // Revisa las credenciales
            // Checar el tipo de usuario que es
            if (login.User == "jose" && login.Pass == "123")
            {
                usuario = new UsuarioModel { Nombre = "Jose", Email = "a@a.com" };
            }
            return usuario;
        }
    }
}