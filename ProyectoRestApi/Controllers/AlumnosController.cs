﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyectoRestApi.Models;

namespace ProyectoRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlumnosController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            //return new string[] { "valor1", "valor2" };
            return StatusCode(200, new PruebaModel { Nombre = "Ka", Edad = 12 });
        }

        // GET api/values/5/3
        [HttpGet("{a}/{b}")]
        public ActionResult<string> Get(int a, int b)
        {
            return "value" + a + b;
        }
        
        // GET api/values/5/3
        [HttpGet("{a}/{b:alpha}")]
        public ActionResult<string> Get(int a, string b)
        {
            return "value" + a + b;
        }

        // GET api/values/5/3
        [HttpGet("productos/{a}/{b}")]
        public ActionResult<string> Productos(int a, int b)
        {
            return "mis productos "+a+b;
        }
    }
}