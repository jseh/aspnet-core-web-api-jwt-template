﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using MySql.Data.MySqlClient;

namespace ProyectoRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestApiController : ControllerBase
    {
        // private const string AuthSchemes = JwtBearerDefaults.AuthenticationScheme;

        // api/testapi/prueba
        [Authorize(Policy = "Alumnos")]
        //[Authorize]
        [HttpGet("prueba")]
        public ActionResult<string> Prueba()
        {
            return "Prueba: Acceso Autorizado + politica ";
        }

        // api/testapi/prueba2
        [Authorize]
        [HttpGet("prueba2")]
        public ActionResult<string> Prueba2()
        {
            return "Prueba: Acceso Autorizado ";
        }

        [HttpGet("dapper")]
        public ActionResult<string> DapperPruebas()
        {
            string sql = "SELECT * FROM usuarios";
            var connectionString = "Server=127.0.0.1;Port=3306;Database=aspcoredb;Uid=root;";
            using (var db = new MySqlConnection(connectionString))
            {
                // consulta
                var datos = db.Query(sql).FirstOrDefault();
                return datos.nombre;
            }


            //return "dadper";

        }

        [HttpGet("mongo")]
        public ActionResult<string> Mongo()
        {

            var mongoClient = new MongoClient("dfgs");
            return "mongo";
        }
    }
}