﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace ProyectoRestApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // JWT Config *********************************
            services.AddAuthentication(

                // Para cookies
                //options =>
                //{
                //    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                //    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                //    options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                //}

                //Para JWT
                //options =>
                //{
                //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                //    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                //}

                //or only

                JwtBearerDefaults.AuthenticationScheme

                )
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters =
                        new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,

                            ValidIssuer = "Jseh",
                            ValidAudience = "Lectores",
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("kjhdlahjdsfaskuiedfqpwfhqweifqñwefji29302139dj23dj293d0jfjasdifjañ")),
                            //ClockSkew = TimeSpan.Zero // remove delay of token when expire
                        };
                });
            // END JWT

            // Politicas de Autorizacion ********************
            services.AddAuthorization(options =>
            {

                options.AddPolicy("Over18", policy =>
                {
                    // ya no es  necesario agragar el scheme donde aplicas la politica
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("TipoUsuario", "Administrativo");

                });

                options.AddPolicy("Alumnos",
                    policy => policy.RequireClaim("TipoUsuario", "Alumno")
                                    .RequireClaim("NoControl"));

                options.AddPolicy("Director",
                    policy => policy.RequireClaim("TipoUsuario", "Administrativo")
                                    .RequireClaim("Usuario"));

                options.AddPolicy("Finacieros",
                    policy => policy.RequireClaim("TipoUsuario", "Financieros")
                                    .RequireClaim("Usuario"));

                options.AddPolicy("Administrativos",
                   policy => policy.RequireClaim("TipoUsuario", "Administrativo")
                                   .RequireClaim("Usuario"));
                
                // Un una sola politica puede comprobar si tiene multiples Claims
                options.AddPolicy("Admins",
                    policy => policy.RequireClaim("TipoUsuario", "Administrador")
                                    .RequireClaim("Usuario"));
               
                // Puede ser “SuperAdministrator” o “ChannelAdministrator” 
                options.AddPolicy("RequireElevatedRights", policy => policy.RequireRole("SuperAdministrator", "ChannelAdministrator"));
            });
            // END













            // agregar esta linea para agregar mvc
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);


            //services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
            //{
            //    builder
            //        .AllowAnyMethod()
            //        .AllowAnyHeader()
            //        .WithOrigins("http://localhost:44386");
            //}));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseCors("CorsPolicy");

            // Se pueden agregar Cors directamente o definiendo una politica primero
            app.UseCors(options => options
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());


            app.UseHttpsRedirection();

            // Ya sea para Cookies o JWT
            app.UseAuthentication();

            //app.UseMvc();
            // agregar esta linea para agregar mvc
            app.UseMvcWithDefaultRoute();
        }
    }
}
